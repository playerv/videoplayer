<?php
include('public/fcm.php');
require_once("public/thumbnail_images.class.php");
include_once('functions.php');
?>

<?php
if (isset($_GET['id'])) {
    $ID = $_GET['id'];
} else {
    $ID = "";
}



if (isset($_POST['btnEdit'])) {
    // delete data from menu table
    $sql_query = "DELETE FROM tbl_playlist_position WHERE playlist_id = ?";

    $stmt = $connect->stmt_init();
    if ($stmt->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt->bind_param('s', $ID);
        // Execute query
        $stmt->execute();
        // store result
        $delete_category_result = $stmt->store_result();
        $stmt->close();
    }

    if (is_array($_POST['position']) || is_object($_POST['position'])) {
        foreach ($_POST['position'] as $key => $val) {
            $position_list[] = $val;
        }
    }

    if (is_array($_POST['nid']) || is_object($_POST['nid'])) {
        foreach ($_POST['nid'] as $key => $val) {
            $nid_list[] = $val;
        }
    }
    for ($j = 0; $j < count($position_list); $j++) {
        $data = array(
            'playlist_id' => $_POST['playlist_id'],
            'position' => $position_list[$j],
            'nid' => $nid_list[$j],
        );

        if ($position_list[$j] != '') {


            $qry = Insert('tbl_playlist_position', $data);
        }
    }
    $_SESSION['msg'] = "";
    header("Location:manage-playlist.php");
    exit;
}


// create array variable to store previous data
$data = array();

$sql_query = "SELECT playlist_id,playlist_title FROM tbl_playlist WHERE playlist_id = ?";

$stmt = $connect->stmt_init();
if ($stmt->prepare($sql_query)) {
    // Bind your variables to replace the ?s
    $stmt->bind_param('s', $ID);
    // Execute query
    $stmt->execute();
    // store result
    $stmt->store_result();
    $stmt->bind_result($data['playlist_id'], $data['playlist_title']
    );
    $stmt->fetch();
    $stmt->close();
}

// create object of functions class
$function = new functions;

// create array variable to store data from database
$datas = array();
if ($_GET['keyword'] != '') {
    if (isset($_GET['keyword'])) {
        // check value of keyword variable
        $keyword = $function->sanitize($_GET['keyword']);
        $bind_keyword = "%" . $keyword . "%";
    } else {
        $keyword = "";
        $bind_keyword = $keyword;
    }

    if (empty($keyword)) {
        $sql_query = "SELECT nid, news_title, news_date, category_name, video_id, content_type FROM tbl_news m, tbl_category c
					WHERE m.cat_id = c.cid  
					ORDER BY m.nid DESC";
    } else {
        $sql_query = "SELECT nid, news_title, news_date, category_name, video_id, content_type FROM tbl_news m, tbl_category c
					WHERE m.cat_id = c.cid AND news_title LIKE ? 
					ORDER BY m.nid DESC";
    }
    echo $sql_query;

    $stmt = $connect->stmt_init();
    if ($stmt->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        if (!empty($keyword)) {
            $stmt->bind_param('s', $bind_keyword);
        }
        // Execute query
        $stmt->execute();
        // store result 
        $stmt->store_result();
        $stmt->bind_result(
                $datas['nid'], $datas['news_title'], $datas['news_date'], $datas['category_name'], $datas['video_id'], $datas['content_type']
        );
        // get total records
        $total_records = $stmt->num_rows;
    }

    // check page parameter
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = 1;
    }

    // number of data that will be display per page		
    $offset = 10;

    //lets calculate the LIMIT for SQL, and save it $from
    if ($page) {
        $from = ($page * $offset) - $offset;
    } else {
        //if nothing was given in page request, lets load the first page
        $from = 0;
    }

    if (empty($keyword)) {
        $sql_query = "SELECT nid, news_title, news_date, cat_id,language_id, video_id, content_type,playlist FROM tbl_news m
					
					ORDER BY m.nid DESC LIMIT ?, ?";
    } else {
        $sql_query = "SELECT nid, news_title, news_date, cat_id, language_id,video_id, content_type,playlist FROM tbl_news m
					WHERE   news_title LIKE ? 
					ORDER BY m.nid DESC LIMIT ?, ?";
    }

    $stmt_paging = $connect->stmt_init();
    if ($stmt_paging->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        if (empty($keyword)) {
            $stmt_paging->bind_param('ss', $from, $offset);
        } else {
            $stmt_paging->bind_param('sss', $bind_keyword, $from, $offset);
        }
        // Execute query
        $stmt_paging->execute();
        // store result 
        $stmt_paging->store_result();
        $stmt_paging->bind_result(
                $data['nid'], $data['news_title'], $data['news_date'], $data['cat_id'], $data['language_id'], $data['video_id'], $data['content_type'], $data['playlist']
        );
        // for paging purpose
        $total_records_paging = $total_records;
    }
}
?>


<section class="content">

    <ol class="breadcrumb">
        <li><a href="dashboard.php">Dashboard</a></li>
        <li class="active">Playlist Position</a></li>
    </ol>

    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">

                        <form method="get">
                            <div class="col-sm-10">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="keyword" placeholder="Search by title...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" name="btnSearch" class="btn bg-blue btn-circle waves-effect waves-circle waves-float"><i class="material-icons">search</i></button>
                            </div>
                        </form>
 <?PHP if(isset($_GET['keyword'])) { ?>
                        <table class='table table-hover table-striped'>
                            <thead>
                                <tr>
                                    <th width="25%">Videos Title</th>
                                    <th width="12%">Videos Image</th>
                                    <th width="12%">Date</th>
                                    <th width="12%">Language Category</th>
                                    <th width="12%">Artist Name</th>
                                    <th width="10%">Playlist </th>
                                    <th width="14%"><center>Action</center></th>
                            </tr>
                            </thead>

<?php 	 while ($stmt_paging->fetch()) { ?>
                                <tr>
                                    <td><?php echo $data['news_title']; ?></td>

                                    <td>
    <?php
    if ($data['content_type'] == 'youtube') {
        ?>
                                            <img src="https://img.youtube.com/vi/<?php echo $data['video_id']; ?>/mqdefault.jpg" height="48px" width="60px"/>
    <?php } else { ?>
                                            <img src="upload/<?php echo $data['news_image']; ?>" height="48px" width="60px"/>
    <?php } ?>
                                    </td>

                                    <td>
                                <?php
                                $date = strtotime($data['news_date']);
                                $new_date = date("F d, Y H:i:s", $date);
                                echo $new_date;
                                ?>
                                    </td>
                                    <td><?php
                                        $language_id = $data['language_id'];
                                        if ($language_id != NULL) {
                                            $sql_language = "SELECT * FROM tbl_language WHERE language_id='$language_id'";
                                            $language_result = mysqli_query($connect, $sql_language);
                                            while ($language_data = mysqli_fetch_array($language_result)) {
                                                $language_name = $language_data['language_name'];
                                            }
                                            echo $language_name;
                                        }
                                        ?>

                                    </td>
                                    <td> <?php
                                    $final_artistlist = "";
                                    $artistlist = $data['cat_id'];
                                    $artistlist_lists = explode(',', $artistlist);
                                    for ($l = 0; $l < count($artistlist_lists); $l++) {
                                        $sql_artistlist = "SELECT * FROM tbl_category WHERE cid='$artistlist_lists[$l]'";
                                        $artistlist_result = mysqli_query($connect, $sql_artistlist);
                                        while ($artist_data = mysqli_fetch_array($artistlist_result)) {
                                            $final_artistlist .= $artist_data['category_name'] . ", ";
                                        }
                                    }
                                    $final_artistlist = substr($final_artistlist, 0, -2);
                                    echo $final_artistlist;
                                    ?></td>
                                    <td>
                                        <?php
                                        $final_playlist = "";
                                        $playlist = $data['playlist'];
                                        $playlist_lists = explode(',', $playlist);
                                        for ($k = 0; $k < count($playlist_lists); $k++) {
                                            $sql_playlist = "SELECT * FROM tbl_playlist WHERE playlist_id='$playlist_lists[$k]'";
                                            $playlist_result = mysqli_query($connect, $sql_playlist);
                                            while ($play_data = mysqli_fetch_array($playlist_result)) {
                                                $final_playlist .= $play_data['playlist_title'] . ", ";
                                            }
                                        }
                                        $final_playlist = substr($final_playlist, 0, -2);
                                        echo $final_playlist;
                                        ?>
                                    </td>
                                    <td><center>
                                    <a href="manage-news.php?send_notification_post=<?php echo $data['nid']; ?>" onclick="return confirm('Send this notification to your users?')">
                                        <i class="material-icons">notifications_active</i>
                                    </a>



                                    <a href="edit-news.php?id=<?php echo $data['nid']; ?>">
                                        <i class="material-icons">mode_edit</i>
                                    </a>

                                    <a href="delete-news.php?id=<?php echo $data['nid']; ?>" onclick="return confirm('Are you sure want to delete this News?')" >
                                        <i class="material-icons">delete</i>
                                    </a></center>
                                </td>
                                </tr>
    <?php
} 
?>
                        </table>


 <?php } ?>
                    </div>	</div>
                <?php if (isset($_GET['id'])) { ?>
                <form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2> Position</h2>
                            <?php echo isset($error['update_category']) ? $error['update_category'] : ''; ?>
                        </div>
                        <div class="body">

                            <div class="row clearfix">

                                <div>
                                    <div class="form-group col-sm-12">
                                        <div class="form-line">
                                            <div class="font-12">Playlist Title</div>
<?php echo $data['playlist_title']; ?>

                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="row" >
                                            <table class='table table-hover table-striped'>
                                                <tr>
                                                    <td><b>Video Name </b></td>
                                                    <td><b> Current Position </b>
                                                </td>
                                                <td> <b>New Position </b> 
                                                    </td>
                                                </tr>
                                                <input type="hidden" class="form-control" name="playlist_id" id="playlist_id" value="<?php echo $ID; ?>"> 
<?php
$sql_playlist = "SELECT n.* from  tbl_playlist_position as p LEFT JOIN tbl_news n ON p.nid = n.nid WHERE p.playlist_id='$ID'";
$playlist_result = mysqli_query($connect, $sql_playlist);
while ($play_data = mysqli_fetch_array($playlist_result)) {

    $playlist_id = $ID;
    $nid = $play_data['nid'];
    $sql_playlist_pos = "SELECT * FROM tbl_playlist_position WHERE playlist_id='$ID' AND nid='$nid'";
    $playlist_pos_result = mysqli_query($connect, $sql_playlist_pos);
    $playlistData = mysqli_fetch_array($playlist_pos_result);
    if (!empty($playlistData)) {
        $position = $playlistData['position'];
    } else {
        $position = '';
    }
    ?>
                                                    <tr>
                                                        <td>  <img src="https://img.youtube.com/vi/<?php echo $play_data['video_id']; ?>/mqdefault.jpg" height="48px" width="60px"/>    <?php echo $play_data['news_title'];
    ?>
                                                            <input type="hidden" class="form-control" name="nid[]"  value="<?php echo $play_data['nid']; ?>"> 
                                                        </td>
                                                         <td>           <input type="text" class="form-control"  style="width:100px;" value="<?php echo $position ?>" readonly> 
                                                        </td>
                                                        <td>           <input type="text" class="form-control" name="position[]"  style="width:100px;" value="<?php echo $position ?>"> 
                                                        </td>
                                                    </tr>
                                                <?PHP }
                                                ?>



                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnEdit">UPDATE</button>
                                    </div>



                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>

    </div>

</section>