<?php
	include 'functions.php';
?>

	<?php 
		// create object of functions class
		$function = new functions;
		
		// create array variable to store data from database
		$data = array();
		
		if(isset($_GET['keyword'])) {	
			// check value of keyword variable
			$keyword = $function->sanitize($_GET['keyword']);
			$bind_keyword = "%".$keyword."%";
		} else {
			$keyword = "";
			$bind_keyword = $keyword;
		}
			
		if (empty($keyword)) {
			$sql_query = "SELECT playlist_id, playlist_title, playlist_image FROM tbl_playlist ORDER BY playlist_id DESC";
		} else {
			$sql_query = "SELECT playlist_id, playlist_title, playlist_image FROM tbl_playlist WHERE playlist_title LIKE ? ORDER BY playlist_id DESC";
		}
		
		
		$stmt = $connect->stmt_init();
		if ($stmt->prepare($sql_query)) {	
			// Bind your variables to replace the ?s
			if (!empty($keyword)) {
				$stmt->bind_param('s', $bind_keyword);
			}
			// Execute query
			$stmt->execute();
			// store result 
			$stmt->store_result();
			$stmt->bind_result( 
					$data['playlist_id'],
					$data['playlist_title'],
					$data['playlist_image']
					);
			// get total records
			$total_records = $stmt->num_rows;
		}
			
		// check page parameter
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
						
		// number of data that will be display per page		
		$offset = 10;
						
		//lets calculate the LIMIT for SQL, and save it $from
		if ($page) {
			$from 	= ($page * $offset) - $offset;
		} else {
			//if nothing was given in page request, lets load the first page
			$from = 0;	
		}	
		
		if (empty($keyword)) {
			$sql_query = "SELECT playlist_id, playlist_title, playlist_image,playlist_category_id FROM tbl_playlist ORDER BY playlist_id DESC LIMIT ?, ?";
		} else {
			$sql_query = "SELECT playlist_id, playlist_title, playlist_image,playlist_category_id FROM tbl_playlist WHERE language_name LIKE ? ORDER BY playlist_id DESC LIMIT ?, ?";
		}
		
		$stmt_paging = $connect->stmt_init();
		if ($stmt_paging ->prepare($sql_query)) {
			// Bind your variables to replace the ?s
			if (empty($keyword)) {
				$stmt_paging ->bind_param('ss', $from, $offset);
			} else {
				$stmt_paging ->bind_param('sss', $bind_keyword, $from, $offset);
			}
			// Execute query
			$stmt_paging ->execute();
			// store result 
			$stmt_paging ->store_result();
			$stmt_paging->bind_result(
				$data['playlist_id'],
				$data['playlist_title'],
				$data['playlist_image'],
                                $data['playlist_category_id']
			);
			// for paging purpose
			$total_records_paging = $total_records; 
		}

		// if no data on database show "No Reservation is Available"
		if ($total_records_paging == 0) {
	
	?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Manage Language</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>MANAGE PLAYLIST</h2>
                            <div class="header-dropdown m-r--5">
                                <a href="add-playlist.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW PLAYLIST</button></a>
                            </div>
                        </div>

                        <div class="body table-responsive">
	                        
	                        <form method="get">
	                        	<div class="col-sm-10">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="keyword" placeholder="Search by Playlist Title...">
										</div>
									</div>
								</div>
								<div class="col-sm-2">
					                <button type="submit" name="btnSearch" class="btn bg-blue btn-circle waves-effect waves-circle waves-float"><i class="material-icons">search</i></button>
								</div>
							</form>
										
							<table class='table table-hover table-striped'>
								<thead>
									<tr>
										<th>Playlist Name</th>
                                                                               	<th>Playlist Image</th>
										<th width="15%">Action</th>
									</tr>
								</thead>

								
							</table>

							<div class="col-sm-10">Wopps! No data found with the keyword you entered.</div>

						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<?php 
		// otherwise, show data
		} else {
			$row_number = $from + 1;
	?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Manage Playlist</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>MANAGE PLAYLIST</h2>
                            <div class="header-dropdown m-r--5">
                                <a href="add-playlist.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW PLAYLIST</button></a>
                            </div>
                        </div>

                        <div class="body table-responsive">
	                        
	                        <form method="get">
	                        	<div class="col-sm-10">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="keyword" placeholder="Search by Playlist Title...">
										</div>
									</div>
								</div>
								<div class="col-sm-2">
					                <button type="submit" name="btnSearch" class="btn bg-blue btn-circle waves-effect waves-circle waves-float"><i class="material-icons">search</i></button>
								</div>
							</form>
										
							<table class='table table-hover table-striped'>
								<thead>
									<tr>
										<th>Playlist Name</th>
										<th>Playlist Image</th>
                                                                                 <th>Playlist Category</th>
                                                                                <th>Total Videos</th>
										<th width="15%">Action</th>
									</tr>
								</thead>

								<?php 
									while ($stmt_paging->fetch()) { ?>
										<tr>
											<td><?php echo $data['playlist_title'];?></td>
											<td><img src="upload/category/<?php echo $data['playlist_image']; ?>" width="48" height="48"/></td>
                                                                                        <td><?php $playlist_category_id=$data['playlist_category_id'];
                                                                                        if ($playlist_category_id !=NULL){
                                                                                        $sql_language = "SELECT * FROM tbl_playlist_category WHERE playlist_category_id='$playlist_category_id'";
                                                                                        $language_result = mysqli_query($connect, $sql_language);
                                                                                        while ($language_data = mysqli_fetch_array($language_result)) { 
                                                                                            $playlist_category_name=$language_data['playlist_category_name']; 
                                                                                        } 
                                                                                        echo $playlist_category_name; }
                                                                                        ?>
                                                                                        
                                                                                        </td>
                                                                                        <td>
                                                                                             <?php
                                       
                                                                                                $playlist_id=$data['playlist_id'];
                                                                                                $sql_playlist = "SELECT count(*) as tot FROM tbl_playlist_position WHERE playlist_id='$playlist_id'";
                                                                                                $playlist_result = mysqli_query($connect, $sql_playlist);
                                                                                                while ($play_data = mysqli_fetch_array($playlist_result)) { 
                                                                                                    $playlist_count =$play_data['tot'];
                                                                                                } 


                                                                                                echo $playlist_count;
                                                                                            ?>
                                                                                            
                                                                                        </td>
                                                                                        
											<td>
                                                                                                     <a href="add-playlist-position.php?id=<?php echo $data['playlist_id'];?>">
									                <i class="material-icons">launch</i>
									            </a>
									            <a href="edit-playlist.php?id=<?php echo $data['playlist_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
									                        
									            <a href="delete-playlist.php?id=<?php echo $data['playlist_id'];?>" onclick="return confirm('Are you sure want to delete this Playlist?')" >
									                <i class="material-icons">delete</i>
									            </a>
                                                                                            
									        </td>
										</tr>
								<?php 
									}
								?>
							</table>

							<h4><?php $function->doPages($offset, 'manage-playlist.php', '', $total_records, $keyword); ?></h4>
							<?php 
								}
							?>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>