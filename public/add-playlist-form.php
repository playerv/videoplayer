<?php include_once('functions.php'); ?>

<?php

if (isset($_POST['btnAdd'])) {

    $playlist_title = $_POST['playlist_title'];
    // create array variable to handle error
    $error = array();
    $sql_category = "SELECT * FROM tbl_playlist where playlist_title='$playlist_title' ";
    $total_category = mysqli_query($connect, $sql_category);
    $total_category = mysqli_fetch_array($total_category);
    if(!empty($total_category)){

      $error['playlist_title'] = " <span class='font-12 col-red'>Duplicate Playlist Name</span>";  
    }
    // get image info
    $menu_image = $_FILES['playlist_image']['name'];
    $image_error = $_FILES['playlist_image']['error'];
    $image_type = $_FILES['playlist_image']['type'];



    if(empty($playlist_title)){
        $error['playlist_title'] = " <span class='label label-danger'>Must Insert!</span>";
    }

    // common image file extensions
    $allowedExts = array("gif", "jpeg", "jpg", "png");

    // get image file extension
    error_reporting(E_ERROR | E_PARSE);
    $extension = end(explode(".", $_FILES["playlist_image"]["name"]));

    if($image_error > 0) {
        $error['playlist_image'] = " <span class='font-12 col-red'>You're not insert images!!</span>";
    } else if(!(($image_type == "image/gif") ||
            ($image_type == "image/jpeg") ||
            ($image_type == "image/jpg") ||
            ($image_type == "image/x-png") ||
            ($image_type == "image/png") ||
            ($image_type == "image/pjpeg")) &&
        !(in_array($extension, $allowedExts))){

        $error['playlist_image'] = " <span class='font-12'>Image type must jpg, jpeg, gif, or png!</span>";
    }

    if(!empty($playlist_title) && empty($error['playlist_title'])){

        // create random image file name
        $string = '0123456789';
        $file = preg_replace("/\s+/", "_", $_FILES['playlist_image']['name']);
        $function = new functions;
        $menu_image = $function->get_random_string($string, 4)."-".date("Y-m-d").".".$extension;

        // upload new image
        $upload = move_uploaded_file($_FILES['playlist_image']['tmp_name'], 'upload/category/'.$menu_image);

        // insert new data to menu table
        $sql_query = "INSERT INTO tbl_playlist (playlist_title, playlist_image)
                        VALUES(?, ?)";

        $upload_image = $menu_image;
        $stmt = $connect->stmt_init();
        if($stmt->prepare($sql_query)) {
            // Bind your variables to replace the ?s
            $stmt->bind_param('ss',
                $playlist_title,
                $upload_image
            );
            // Execute query
            $stmt->execute();
            // store result
            $result = $stmt->store_result();
            $stmt->close();
        }

        if($result) {
            $error['add_category'] = "<br><div class='alert alert-info'>New Playlist Added Successfully...</div>";
        } else {
            $error['add_category'] = "<br><div class='alert alert-danger'>Added Failed</div>";
        }
    }

}
$sql_category = "SELECT * FROM tbl_playlist_category ORDER BY playlist_category_id ASC";
$category_result = mysqli_query($connect, $sql_category);
?> 

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="manage-playlist.php">Manage Playlist</a></li>
            <li class="active">Add Playlist</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>ADD ARTIST</h2>
                                <?php echo isset($error['add_category']) ? $error['add_category'] : '';?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="playlist_title" id="playlist_title" required>
                                            <label class="form-label">Playlist Title</label>
                                            <div class="div-error"><?php echo isset($error['playlist_title']) ? $error['playlist_title'] : '';?></div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="font-12">Playlist Category *</div>
                                        <select class="form-control show-tick" name="playlist_category_id" id="playlist_category_id">
                                        <?php while ($datas = mysqli_fetch_array($category_result)) { ?>
                                                <option value="<?php echo $datas['playlist_category_id']; ?>"><?php echo $datas['playlist_category_name']; ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input type="file" name="playlist_image" id="playlist_image" id="playlist_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" />
                                                <div class="div-error"><?php echo isset($error['playlist_image']) ? $error['playlist_image'] : '';?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>

                                   
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>