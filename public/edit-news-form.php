<style>

    .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {

        position: absolute;
        margin-top: 4px \9;
        margin-left: -20px;
        left: 18px;
        opacity: 1 !important;

    }

</style>
<?php
include 'fcm.php';
include 'functions.php';
require 'public/thumbnail_images.class.php';

if (isset($_GET['id'])) {

    $qry = "SELECT * FROM tbl_news WHERE nid ='" . $_GET['id'] . "'";
    $result = mysqli_query($connect, $qry);
    $row = mysqli_fetch_assoc($result);
}

if (isset($_POST['submit'])) {
    $video_id = 'cda11up';
    if ($_POST['upload_type'] == 'Upload') {
        if ($_FILES['news_image']['name'] != '') {
            unlink('upload/' . $_POST['old_image']);
            $news_image = time() . '_' . $_FILES['news_image']['name'];
            $pic2 = $_FILES['news_image']['tmp_name'];
            $tpath2 = 'upload/' . $news_image;
            copy($pic2, $tpath2);
        } else {
            $news_image = $_POST['old_image'];
        }

        if ($_FILES['video']['name'] != '') {

            unlink('upload/video' . $_POST['old_video']);
            $video = time() . '_' . $_FILES['video']['name'];
            $pic1 = $_FILES['video']['tmp_name'];
            $tpath1 = 'upload/video/' . $video;
            copy($pic1, $tpath1);

            $bytes = $_FILES['video']['size'];

            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2) . ' GB';
            } else if ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2) . ' MB';
            } else if ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2) . ' KB';
            } else if ($bytes > 1) {
                $bytes = $bytes . ' bytes';
            } else if ($bytes == 1) {
                $bytes = $bytes . ' byte';
            } else {
                $bytes = '0 bytes';
            }
        } else {
            $bytes = $_POST['old_size'];
            $video = $_POST['old_video'];
        }
    } else if ($_POST['upload_type'] == 'Url') {

        if ($_FILES['image']['name'] != '') {
            unlink('upload/' . $_POST['old_image']);
            $news_image = time() . '_' . $_FILES['image']['name'];
            $pic2 = $_FILES['image']['tmp_name'];
            $tpath2 = 'upload/' . $news_image;
            copy($pic2, $tpath2);
        } else {
            $news_image = $_POST['old_image'];
        }

        $video = $_POST['url_source'];
    } else if ($_POST['upload_type'] == 'Post') {

        if ($_FILES['post_image']['name'] != '') {
            unlink('upload/' . $_POST['old_image']);
            $news_image = time() . '_' . $_FILES['post_image']['name'];
            $pic2 = $_FILES['post_image']['tmp_name'];
            $tpath2 = 'upload/' . $news_image;
            copy($pic2, $tpath2);
        } else {
            $news_image = $_POST['old_image'];
        }

        /**
         * multiple upload
         */
        $imageNames = array();
        $imageFiles = functions::reArrayFiles($_FILES['imageoption']);


        foreach ($imageFiles as $imageFile) {
            if ($imageFile['error'] == 0) {
                $newName = time() . '_' . $imageFile['name'];
                $img = $imageFile['tmp_name'];
                $imgPath = 'upload/' . $newName;
                copy($img, $imgPath);

                $imageNames[] = $newName;
            }
        }

        $video = $_POST['url_source'];
    } else {
        $bytes = '';
        $video = $_POST['youtube'];
        $news_image = '';

        function youtube_id_from_url($url) {

            $pattern = '%^# Match any youtube URL
		        (?:https?://)?  # Optional scheme. Either http or https
		        (?:www\.)?      # Optional www subdomain
		        (?:             # Group host alternatives
		          youtu\.be/    # Either youtu.be,
		        | youtube\.com  # or youtube.com
		          (?:           # Group path alternatives
		            /embed/     # Either /embed/
		          | /v/         # or /v/
		          | /watch\?v=  # or /watch\?v=
		          )             # End path alternatives.
		        )               # End host alternatives.
		        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
		        $%x'
            ;

            $result = preg_match($pattern, $url, $matches);

            if (false !== $result) {
                return $matches[1];
            }

            return false;
        }

        $video_id = youtube_id_from_url($_POST['youtube']);
    }
    $cat_id_list = "";
        if (is_array($_POST['cat_id']) || is_object($_POST['cat_id'])) {
            foreach ($_POST['cat_id'] as $key => $val) {
                $cat_id_list .= $val . ",";
            }
            $cat_id_list = substr($cat_id_list, 0, -1);
        }
        $playlist_id_list = "";
        if (is_array($_POST['position']) || is_object($_POST['position'])) {
            foreach ($_POST['position'] as $key => $val) {
                $position_list[] = $val;
            }
        }
         if (is_array($_POST['playlist_id']) || is_object($_POST['playlist_id'])) {
            foreach ($_POST['playlist_id'] as $key => $val) {
                $playlist_list[] = $val;
            }
        }
        
            for ($k = 0; $k < count($position_list); $k++) {   
        if ($position_list[$k] != '') {
            $playlist_id_list .= $playlist_list[$k] . ",";
        }
            }

            $playlist_id_list = substr($playlist_id_list, 0, -1);

    $data = array(
        'cat_id' => $cat_id_list,
        'language_id' => $_POST['language_id'],
        'news_title' => addslashes($_POST['news_title']),
        'video_url' => $video,
        'video_id' => $video_id,
        'news_description' => addslashes($_POST['news_description']),
        'playlist' => $playlist_id_list,
        'content_type' => $_POST['upload_type'],
    );

    $hasil = Update('tbl_news', $data, "WHERE nid = '" . $_POST['id'] . "'");

    // Playlist Position start Here 

    $last_id = $_POST['id'];
    $sql_query = "DELETE FROM tbl_playlist_position WHERE nid = ?";

    $stmt = $connect->stmt_init();
    if ($stmt->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt->bind_param('s', $last_id);
        // Execute query
        $stmt->execute();
        // store result
        $delete_category_result = $stmt->store_result();
        $stmt->close();
    }


   
    for ($j = 0; $j < count($position_list); $j++) {
        $data = array(
            'playlist_id' => $playlist_list[$j],
            'position' => $position_list[$j],
            'nid' => $last_id,
        );

        if ($position_list[$j] != '') {
            $qry = Insert('tbl_playlist_position', $data);
        }
    }

    if ($hasil > 0) {
        if (isset($imageNames) && count($imageNames) > 0) {
            global $config;
            $last_id = $_POST['id'];
            $multi_sql = "INSERT INTO tbl_news_gallery (nid, image_name) VALUE ";
            foreach ($imageNames as $imageName) {
                $multi_sql .= "('$last_id', '$imageName'),";
            }
            $multi_sql = trim($multi_sql, ',');
            mysqli_query($config, $multi_sql);
        }
        $_SESSION['msg'] = "";
        header("Location:edit-news.php?id=" . $_POST['id']);
        exit;
    }
}

$sql_query = "SELECT * FROM tbl_category ORDER BY category_name ASC";
$category_result = mysqli_query($connect, $sql_query);

$sql_language = "SELECT * FROM tbl_language ORDER BY language_id DESC";
$language_result = mysqli_query($connect, $sql_language);

$sql_playlist = "SELECT * FROM tbl_playlist ORDER BY playlist_id DESC";
$playlist_result = mysqli_query($connect, $sql_playlist);
?>

<script type="text/javascript">

    $(document).ready(function (e) {
        $("#upload_type").change(function () {
            var type = $("#upload_type").val();

            if (type == "youtube") {
                $("#video_upload").hide();
                $("#video_post").hide();
                $("#direct_url").hide();
                $("#youtube").show();
            }

            if (type == "Post") {
                $("#youtube").hide();
                $("#video_upload").hide();
                $("#direct_url").hide();
                $("#video_post").show();

                $("#multiple_images").hide();
            }

            if (type == "Url") {
                $("#youtube").hide();
                $("#video_upload").hide();
                $("#video_post").hide();
                $("#direct_url").show();
            }

            if (type == "Upload") {
                $("#youtube").hide();
                $("#video_post").hide();
                $("#direct_url").hide();
                $("#video_upload").show();
            }

        });

        $(window).load(function () {
            var type = $("#upload_type").val();

            if (type == "youtube") {
                $("#video_upload").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#youtube").show();
            }

            if (type == "Url") {
                $("#youtube").hide();
                $("#video_upload").hide();
                $("#video_post").hide();
                $("#direct_url").show();
            }

            if (type == "Upload") {
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#video_upload").show();
            }

            if (type == "Post") {
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_upload").hide();
                $("#video_post").show();

                $("#multiple_images").hide();
            }

        });

    });

</script>

<section class="content">

    <ol class="breadcrumb">
        <li><a href="dashboard.php">Dashboard</a></li>
        <li><a href="manage-news.php">Manage Videos</a></li>
        <li class="active">Edit Videos</a></li>
    </ol>

    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT VIDEOS</h2>
<?php if (isset($_SESSION['msg'])) { ?>
                                <br><div class='alert alert-info'>Videos Successfully Updated...</div>
                                <?php unset($_SESSION['msg']);
                            }
                            ?>
                        </div>
                        <div class="body">

                            <div class="row clearfix">

                                <div class="col-sm-5">

                                    <div class="form-group">
                                        <div class="font-12">Videos Title</div>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="news_title" id="news_title" placeholder="Videos Title" value="<?php echo $row['news_title']; ?>" required>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="font-12">Language Category </div>
                                        <select class="form-control show-tick" name="language_id" id="language_id">
                                            <?php
                                            while ($r_c_row = mysqli_fetch_array($language_result)) {
                                                $sel = '';
                                                if ($r_c_row['language_id'] == $row['language_id']) {
                                                    $sel = "selected";
                                                }
                                                ?>
                                                <option value="<?php echo $r_c_row['language_id']; ?>" <?php echo $sel; ?>><?php echo $r_c_row['language_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <div class="font-12">Artist</div>
                                        <div class="row" style=" height: 150px;overflow: scroll;">

                                            <?php
                                            while ($cdata = mysqli_fetch_array($category_result)) {
                                                $cat_id_list = $row['cat_id'];
                                                $cat_id_lists = explode(',', $cat_id_list);
                                                ?>
                                                <div class="col-md-6">
                                                    <div class="checkbox">
                                                        <?php
                                                        if (in_array($cdata['cid'], $cat_id_lists)) {
                                                            ?> <label><input type="checkbox" value="<?php echo $cdata['cid']; ?>" name="cat_id[]" checked=""><?php echo $cdata['category_name']; ?></label> <?PHP
                                                        } else {
                                                            ?> <label><input type="checkbox" value="<?php echo $cdata['cid']; ?>" name="cat_id[]" ><?php echo $cdata['category_name']; ?></label> <?PHP
                                                            }
                                                            ?>

                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>


                                    </div>
                                    <div class="form-group">Playlist Section *</div>
                                    <div class="row" style=" height: 150px;overflow: scroll;">
                                        <table class='table table-hover table-striped' >
                                            <tr>
                                                <td> <b>Playlist Name </b>
                                                </td>
                                                <td><b> Current Position </b>
                                                </td>
                                                <td><b> New Position </b>
                                                </td>
                                            </tr>



                                            <?php
                                            while ($pdata = mysqli_fetch_array($playlist_result)) {

                                                $playlist_id = $pdata['playlist_id'];
                                                $nid = $row['nid'];
                                                $sql_playlist_pos = "SELECT * FROM tbl_playlist_position WHERE playlist_id='$playlist_id' AND nid='$nid'";
                                                $playlist_pos_result = mysqli_query($connect, $sql_playlist_pos);
                                                $playlistData = mysqli_fetch_array($playlist_pos_result);
                                                if (!empty($playlistData)) {
                                                    $position = $playlistData['position'];
                                                } else {
                                                    $position = '';
                                                }
                                                ?>
                                                <tr>

                                                    <td>    <?php echo $pdata['playlist_title']; ?>
                                                          <input type="hidden" class="form-control" name="playlist_id[]"  value="<?php echo $pdata['playlist_id']; ?>"> 
                                                        
                                                    </td>
                                                    <td>           <input type="text" class="form-control"    style="width:80px;" value="<?php echo $position; ?>" readonly=""> 
                                                    </td>
                                                    <td>           <input type="text" class="form-control" name="position[]"  style="width:100px;" value="<?php echo $position; ?>"> 
                                                    </td>
                                                </tr>
<?PHP }
?>



                                        </table>
                                    </div>


                                    <div class="form-group">
                                        <div class="font-12">Content Type</div>
                                        <select class="form-control show-tick" name="upload_type" id="upload_type">
                                            <option <?php
if ($row['content_type'] == 'Post') {
    echo 'selected';
}
?> value="Post">Standard Post</option>
                                            <option <?php
                                            if ($row['content_type'] == 'youtube') {
                                                echo 'selected';
                                            }
                                            ?> value="youtube">Video Post (YouTube)</option>
                                            <option <?php
                                            if ($row['content_type'] == 'Url') {
                                                echo 'selected';
                                            }
                                            ?> value="Url">Video Post (Url)</option>
                                            <option <?php
                                                if ($row['content_type'] == 'Upload') {
                                                    echo 'selected';
                                                }
                                            ?> value="Upload">Video Post (Upload)</option>
                                        </select>
                                    </div>

                                    <div id="video_post">

                                        <div class="font-12 ex1">Image Primary ( jpg / png ) *</div>
                                        <div class="form-group">
                                            <input type="file" name="post_image" id="post_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" data-default-file="upload/<?php echo $row['news_image']; ?>" data-show-remove="false"/>
                                            <div class="div-error"><?php echo isset($error['post_image']) ? $error['post_image'] : ''; ?></div>
                                        <!-- <input type="file" name="post_image" id="post_image" /> -->
                                        </div>



                                        <div id="multiple_images">
                                            <div>
                                                <?php
                                                while ($wall_row = mysqli_fetch_array($wall_result)) {
                                                    ?>

                                                    <img src="upload/<?php echo $wall_row['image_name']; ?>" width="80px" alt="image">
                                                    <a href="delete-image.php?id=<?php echo $wall_row['id']; ?>" onclick="return confirm('Are you sure want to delete this image?')"><img id="img" src="assets/images/x.png" alt="delete"></a>

<?php } ?>
                                            </div>

                                            <div>
                                                <!-- <div class="font-12 ex1">Image Optional ( jpg / png )</div> -->
                                                <div class="form-group">
                                                    <input type="hidden" name="imageoption[]" id="imageoptions"/>
                                                </div>
                                                <div class="multiupload"></div>
                                                <br>
                                                <input type="hidden" class="btn bg-blue waves-effect" id="addnewUpload" value="add more" />
                                            </div>
                                        </div>

                                    </div>

                                    <div id="youtube">
                                        <div class="form-group">
                                            <div class="font-12">Youtube URL</div>
                                            <div class="form-line">
                                                <input type="url" class="form-control" name="youtube" id="youtube" placeholder="https://www.youtube.com/watch?v=33F5DJw3aiU" value="<?php echo $row['video_url']; ?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="direct_url">
                                        <div class="form-group">
                                            <input type="file" name="image" id="image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" data-default-file="upload/<?php echo $row['news_image']; ?>" data-show-remove="false"/>
                                        </div>
                                        <div class="form-group">
                                            <div class="font-12">Video URL</div>
                                            <div class="form-line">
                                                <input type="url" class="form-control" name="url_source" id="url_source" placeholder="http://www.xyz.com/news_title.mp4" value="<?php echo $row['video_url']; ?>" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="video_upload">
                                        <div class="form-group">
                                            <input type="file" id="news_image" name="news_image" id="news_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" data-default-file="upload/<?php echo $row['news_image']; ?>" data-show-remove="false" />
                                        </div>

                                        <div class="form-group">
                                            <input type="file" id="video" name="video" id="video" class="dropify-video" data-allowed-file-extensions="3gp mp4 mpg wmv mkv m4v mov flv" data-default-file="upload/<?php echo $row['video_url']; ?>" data-show-remove="false" />
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <textarea class="form-control" name="news_description" id="news_description" class="form-control" cols="60" rows="10" required><?php echo $row['news_description']; ?></textarea>
                                        <?php if ($ENABLE_RTL_MODE == 'true') { ?>
                                            <script>
                                                CKEDITOR.replace('news_description');
                                                CKEDITOR.config.contentsLangDirection = 'rtl';
                                            </script>
                                        <?php } else { ?>
                                            <script>
                                                CKEDITOR.replace('news_description');
                                             </script>
                                        <?php } ?>
                                    </div>

                                    <input type="hidden" name="old_image" value="<?php echo $row['news_image']; ?>">
                                    <input type="hidden" name="old_video" value="<?php echo $row['video_url']; ?>">
                                    <input type="hidden" name="old_size" value="<?php echo $row['size']; ?>">
                                    <input type="hidden" name="id" value="<?php echo $row['nid']; ?>">

                                    <button type="submit" name="submit" class="btn bg-blue waves-effect pull-right">UPDATE</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

</section>