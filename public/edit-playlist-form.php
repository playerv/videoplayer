<?php include_once('functions.php'); ?>

<?php
    if (isset($_GET['id'])) {
        $ID = $_GET['id'];
    } else {
        $ID = "";
    }
  // create array variable to store category data
    $category_data = array();

    $sql_query = "SELECT playlist_image
                    FROM tbl_playlist
                    WHERE playlist_id = ?";

    $stmt_category = $connect->stmt_init();
    if ($stmt_category->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt_category->bind_param('s', $ID);
        // Execute query
        $stmt_category->execute();
        // store result
        $stmt_category->store_result();
        $stmt_category->bind_result($previous_category_image);
        $stmt_category->fetch();
        $stmt_category->close();
    }


    if (isset($_POST['btnEdit'])) {
        $playlist_title = $_POST['playlist_title']; 
        $playlist_category_id = $_POST['playlist_category_id'];

        // get image info
        $menu_image = $_FILES['playlist_image']['name'];
        $image_error = $_FILES['playlist_image']['error'];
        $image_type = $_FILES['playlist_image']['type'];

        // create array variable to handle error
        $error = array();

        if (empty($playlist_title)) {
            $error['playlist_title'] = " <span class='label label-danger'>Must Insert!</span>";
        }

        // common image file extensions
        $allowedExts = array("gif", "jpeg", "jpg", "png");

        // get image file extension
        error_reporting(E_ERROR | E_PARSE);
        $extension = end(explode(".", $_FILES["playlist_image"]["name"]));

        if (!empty($menu_image)) {
            if (!(($image_type == "image/gif") ||
                    ($image_type == "image/jpeg") ||
                    ($image_type == "image/jpg") ||
                    ($image_type == "image/x-png") ||
                    ($image_type == "image/png") ||
                    ($image_type == "image/pjpeg")) &&
                !(in_array($extension, $allowedExts))
            ) {

                $error['playlist_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            }
        }

        if (!empty($playlist_title) && empty($error['playlist_title'])) {

            if (!empty($menu_image)) {

                // create random image file name
                $string = '0123456789';
                $file = preg_replace("/\s+/", "_", $_FILES['playlist_image']['name']);
                $function = new functions;
                $category_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;

                // delete previous image
                $delete = unlink('upload/category/' . "$previous_category_image");

                // upload new image
                $upload = move_uploaded_file($_FILES['playlist_image']['tmp_name'], 'upload/category/' . $category_image);

                $sql_query = "UPDATE tbl_playlist
                                SET playlist_title = ?, playlist_image = ?, playlist_category_id = ?
                                WHERE playlist_id = ?";

                $upload_image = $category_image;
                $stmt = $connect->stmt_init();
                if ($stmt->prepare($sql_query)) {
                    // Bind your variables to replace the ?s
                    $stmt->bind_param('ssss',
                        $playlist_title,
                        $upload_image,
                        $playlist_category_id,
                        $ID);
                    // Execute query
                    $stmt->execute();
                    // store result
                    $update_result = $stmt->store_result();
                    $stmt->close();
                }
            } else {

                 $sql_query = "UPDATE tbl_playlist
                                SET playlist_title = ?, playlist_category_id = ?
                                WHERE playlist_id = ?";

                $stmt = $connect->stmt_init();
                if ($stmt->prepare($sql_query)) {
                    // Bind your variables to replace the ?s
                    $stmt->bind_param('sss',
                        $playlist_title,
                        $playlist_category_id,
                        $ID);
                    // Execute query
                    $stmt->execute();
                    // store result
                    $update_result = $stmt->store_result();
                    $stmt->close();
                }
            }

            // check update result
            if ($update_result) {
                $error['update_category'] = "<br><div class='alert alert-info'>Playlist Updated Successfully...</div>";
            } else {
                $error['update_category'] = "<br><div class='alert alert-danger'>Update Failed</div>";
            }

        }
    }
    

    // create array variable to store previous data
    $data = array();

    $sql_query = "SELECT playlist_id,playlist_title,playlist_image,playlist_category_id FROM tbl_playlist WHERE playlist_id = ?";

    $stmt = $connect->stmt_init();
    if ($stmt->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt->bind_param('s', $ID);
        // Execute query
        $stmt->execute();
        // store result
        $stmt->store_result();
        $stmt->bind_result($data['playlist_id'],
            $data['playlist_title'],
            $data['playlist_image'],
            $data['playlist_category_id']);
        $stmt->fetch();
        $stmt->close();
    }

$sql_language = "SELECT * FROM tbl_playlist_category ORDER BY playlist_category_id DESC";
$language_result = mysqli_query($connect, $sql_language);   

?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="manage-playlist.php">Manage Playlist</a></li>
            <li class="active">Edit Playlist</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT PLAYLIST</h2>
                                <?php echo isset($error['update_category']) ? $error['update_category'] : ''; ?>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                
                                <div>
                                    <div class="form-group col-sm-12">
                                        <div class="form-line">
                                            <div class="font-12">Playlist Title</div>
                                            <input type="text" class="form-control" name="playlist_title" id="playlist_title" value="<?php echo $data['playlist_title']; ?>" required>
                                            <!-- <label class="form-label">Category Name</label> -->
                                        </div>
                                    </div>
                                       <div class="form-group col-sm-12">
                                        <div class="font-12">Playlist Category </div>
                                        <select class="form-control show-tick" name="playlist_category_id" id="playlist_category_id" >
<?php
while ($r_c_row = mysqli_fetch_array($language_result)) {
    $sel = '';
    if ($r_c_row['playlist_category_id'] == $data['playlist_category_id']) {
        $sel = "selected";
    }
    ?>
                                                <option value="<?php echo $r_c_row['playlist_category_id']; ?>" <?php echo $sel; ?>><?php echo $r_c_row['playlist_category_name']; ?></option>
<?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input type="file" name="playlist_image" id="playlist_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" data-default-file="upload/category/<?php echo $data['playlist_image']; ?>" data-show-remove="false"/>
                                                <div class="div-error"><?php echo isset($error['playlist_image']) ? $error['playlist_image'] : '';?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnEdit">UPDATE</button>
                                    </div>

                                   
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>