<?php include_once('functions.php'); ?>

<?php
    if (isset($_GET['id'])) {
        $ID = $_GET['id'];
    } else {
        $ID = "";
    }
  // create array variable to store category data
    $category_data = array();

    $sql_query = "SELECT language_image
                    FROM tbl_language
                    WHERE language_id = ?";

    $stmt_category = $connect->stmt_init();
    if ($stmt_category->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt_category->bind_param('s', $ID);
        // Execute query
        $stmt_category->execute();
        // store result
        $stmt_category->store_result();
        $stmt_category->bind_result($previous_category_image);
        $stmt_category->fetch();
        $stmt_category->close();
    }


    if (isset($_POST['btnEdit'])) {
        $language_name = $_POST['language_name']; 

        // get image info
        $menu_image = $_FILES['language_image']['name'];
        $image_error = $_FILES['language_image']['error'];
        $image_type = $_FILES['language_image']['type'];

        // create array variable to handle error
        $error = array();

        if (empty($language_name)) {
            $error['language_name'] = " <span class='label label-danger'>Must Insert!</span>";
        }

        // common image file extensions
        $allowedExts = array("gif", "jpeg", "jpg", "png");

        // get image file extension
        error_reporting(E_ERROR | E_PARSE);
        $extension = end(explode(".", $_FILES["language_image"]["name"]));

        if (!empty($menu_image)) {
            if (!(($image_type == "image/gif") ||
                    ($image_type == "image/jpeg") ||
                    ($image_type == "image/jpg") ||
                    ($image_type == "image/x-png") ||
                    ($image_type == "image/png") ||
                    ($image_type == "image/pjpeg")) &&
                !(in_array($extension, $allowedExts))
            ) {

                $error['language_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            }
        }

        if (!empty($language_name) && empty($error['language_image'])) {

            if (!empty($menu_image)) {

                // create random image file name
                $string = '0123456789';
                $file = preg_replace("/\s+/", "_", $_FILES['language_image']['name']);
                $function = new functions;
                $category_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;

                // delete previous image
                $delete = unlink('upload/category/' . "$previous_category_image");

                // upload new image
                $upload = move_uploaded_file($_FILES['language_image']['tmp_name'], 'upload/category/' . $category_image);

                $sql_query = "UPDATE tbl_language
                                SET language_name = ?, language_image = ?
                                WHERE language_id = ?";

                $upload_image = $category_image;
                $stmt = $connect->stmt_init();
                if ($stmt->prepare($sql_query)) {
                    // Bind your variables to replace the ?s
                    $stmt->bind_param('sss',
                        $language_name,
                        $upload_image,
                        $ID);
                    // Execute query
                    $stmt->execute();
                    // store result
                    $update_result = $stmt->store_result();
                    $stmt->close();
                }
            } else {

                $sql_query = "UPDATE tbl_language
                                SET language_name = ?
                                WHERE language_id = ?";

                $stmt = $connect->stmt_init();
                if ($stmt->prepare($sql_query)) {
                    // Bind your variables to replace the ?s
                    $stmt->bind_param('ss',
                        $language_name,
                        $ID);
                    // Execute query
                    $stmt->execute();
                    // store result
                    $update_result = $stmt->store_result();
                    $stmt->close();
                }
            }

            // check update result
            if ($update_result) {
                $error['update_category'] = "<br><div class='alert alert-info'>Language Updated Successfully...</div>";
            } else {
                $error['update_category'] = "<br><div class='alert alert-danger'>Update Failed</div>";
            }

        }
    }
    

    // create array variable to store previous data
    $data = array();

    $sql_query = "SELECT language_id,language_name,language_image FROM tbl_language WHERE language_id = ?";

    $stmt = $connect->stmt_init();
    if ($stmt->prepare($sql_query)) {
        // Bind your variables to replace the ?s
        $stmt->bind_param('s', $ID);
        // Execute query
        $stmt->execute();
        // store result
        $stmt->store_result();
        $stmt->bind_result($data['language_id'],
            $data['language_name'],
            $data['language_image']        );
        $stmt->fetch();
        $stmt->close();
    }
    

?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="manage-category.php">Manage Language</a></li>
            <li class="active">Edit Language</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT Language</h2>
                                <?php echo isset($error['update_category']) ? $error['update_category'] : ''; ?>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                
                                <div>
                                    <div class="form-group col-sm-12">
                                        <div class="form-line">
                                            <div class="font-12">Language Name</div>
                                            <input type="text" class="form-control" name="language_name" id="language_name" value="<?php echo $data['language_name']; ?>" required>
                                            <!-- <label class="form-label">Category Name</label> -->
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input type="file" name="language_image" id="language_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" data-default-file="upload/category/<?php echo $data['language_image']; ?>" data-show-remove="false"/>
                                                <div class="div-error"><?php echo isset($error['language_image']) ? $error['language_image'] : '';?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnEdit">UPDATE</button>
                                    </div>

                                   
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>